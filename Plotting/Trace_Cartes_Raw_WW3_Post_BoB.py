#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 29 09:59:38 2018

@author: louis
"""
import glob,os,sys,pickle
from datetime import datetime
import pytz
import numpy
from numpy import *
import scipy.signal as sig
from matplotlib.pyplot import *
import matplotlib.ticker as mticker
import matplotlib.animation as manimation
import cartopy
from mpl_toolkits.axes_grid1 import make_axes_locatable
from numpy_groupies import aggregate

import cartopy.crs as ccrs
from netCDF4 import Dataset

rcParams['font.family']='sans-serif'
rcParams['font.sans-serif']=['Helvetica Neue LT']

minLat=43-0.001;
maxLat=50+0./60+1e-6;
minLon=-8-0./60;
maxLon=-1-0./60+1e-6;

if not os.path.isfile('bathy_BoB.dat'):
    f_bathy=Dataset('MARC_F1-MARS3D-MANGAE2500_SEASTAREX_20220525.nc','r')
    f_bathy.set_auto_mask(False)
    lat_bathy=f_bathy['latitude'][:,:]
    lon_bathy=f_bathy['longitude'][:,:]
    z_bathy=-f_bathy['H0'][:,:]
    f_bathy.close()
    pickle.dump((lat_bathy,lon_bathy,z_bathy),open('bathy_BoB.dat','wb'),-1)
else:
    (lat_bathy,lon_bathy,z_bathy)=pickle.load(open('bathy_BoB.dat','rb'))

dgb=lon_bathy[0,1]-lon_bathy[0,0]
dlb=lat_bathy[1,0]-lat_bathy[0,0]

geo=ccrs.PlateCarree()
prj=ccrs.TransverseMercator(central_longitude=(minLon+maxLon)/2,central_latitude=(minLat+maxLat)/2)

def lonfmt(x,y):
    if np.abs(x-np.round(x))<1./60:
        return "%d°%c"%(np.abs(np.round(x)),{1:'E',-1:'W',0:'E'}[np.sign(x)])
    else:
        return "%d'"%(np.round(np.abs(x)*60)%60)

def latfmt(x,y):
    if np.abs(x-np.round(x))<1./60:
        return "%d°%c"%(np.abs(np.round(x)),{1:'N',-1:'S'}[np.sign(x)])
    else:
        return "%d'"%(np.round(np.abs(x)*60)%60)
    
#lonfmt=lambda x,y:"%d\u00B0%d'%c"%(abs(((x+180)%360-180)),round((abs((x+180)%360-180)%1)*60),'E' if ((x+180)%360)-180>=0 else 'W')
#latfmt=lambda x,y:"%d\u00B0%d'%c"%(abs(x),round((abs(x)%1)*60),'N' if x>=0 else 'S')

dpi=200
if os.path.exists('coastline_BoB.dat'):
    m=pickle.load(open('coastline_BoB.dat','rb'))
    dpi=m.figure.get_dpi()
    
else:
    figure(1,dpi=dpi,clear=True)
    m=axes(projection=prj)

    m.set_extent([minLon-1e-3,maxLon+1e-3,minLat-1e-3,maxLat+1e-3],crs=geo)

    hc=m.add_feature(cartopy.feature.GSHHSFeature('f',levels=[1,2]),edgecolor='black',facecolor=0.7*np.array([1,1,1]),zorder=10,linewidth=0.5,rasterized=True)
    pickle.dump(m,open('coastline_BoB.dat','wb'),-1)
    
m.set_extent([minLon-1e-3,maxLon+1e-3,minLat-1e-3,maxLat+1e-3],crs=geo)
hp=m.pcolormesh(lon_bathy-dgb/2,lat_bathy-dlb/2,z_bathy,cmap='gray',vmin=-200,vmax=50,transform=geo,rasterized=True)
hco=m.contour(lon_bathy,lat_bathy,z_bathy,[-5000,-4000,-3000,-2000,-1000,-500,-150,-100,-50],colors='k',linestyles='solid',linewidths=0.5,transform=geo,rasterized=True)
gl=m.gridlines(xlocs=np.arange(minLon,maxLon+1,1),
               ylocs=np.arange(minLat,maxLat,1),
               xformatter=mticker.FuncFormatter(lonfmt),
               yformatter=mticker.FuncFormatter(latfmt),
               draw_labels=True,
               color='k',
               linewidth=0.5,
               linestyle=(0,(1,3)),zorder=20)

gl.xlabel_style={'size':16}
gl.ylabel_style={'size':16}
gl.xpadding=18
gl.ypadding=10
gl.top_labels=False
gl.right_labels=False
gl.rotate_labels=False

m.text(-0.11, 0.50,'Lat (°)',fontsize=16,va='bottom', ha='center',rotation='vertical', rotation_mode='anchor',transform=m.transAxes)
m.text(0.50,-0.10 ,'Long (°)',fontsize=16,va='bottom', ha='center',transform=m.transAxes)


f=Dataset('WW3_NORGAS-UG_SEASTAREX_20220525.nc','r')
f.set_auto_mask(False)
t=np.array([datetime(1990,1,1,tzinfo=pytz.utc).timestamp()+tt*86400 for tt in f.variables['time'][:]])

Nd=8;SF=0.1;
SF=0.5;
hs=m.quiver(np.array([-3.5]),np.array([48.25]),
            np.array([0.1*SF]),np.array([0]),transform=geo,scale=1.0,pivot='middle',zorder=20)
ht=text(-3,48.25,'0.1 m/s',transform=geo,fontsize=16,horizontalalignment='left',verticalalignment='center',zorder=20)
#hm=m.plot(-2,48.2,'r^',transform=geo,markersize=10,mfc='w',mew=2)


tri=None
hi=None

for u in range(0,len(t),1):
    tc=t[u]
        
    Ut=f['uuss'][u,:]
    Vt=f['vuss'][u,:]
    Hs=f['hs'][u,:]

    ibons=np.where(Hs>0)[0]
    Lon=f['longitude'][:]
    Lat=f['latitude'][:]
    Lon=Lon[ibons]
    Lat=Lat[ibons]
    tri=matplotlib.tri.Triangulation(Lon,Lat)
        
    Hs=Hs[ibons]
    Ut=Ut[ibons]
    Vt=Vt[ibons]
    
    Ut[Ut<-20]=np.NaN
    Vt[Vt<-20]=np.NaN
    
    U = matplotlib.tri.LinearTriInterpolator(tri,Ut)(lon_bathy[::Nd,::Nd],lat_bathy[::Nd,::Nd])
    V = matplotlib.tri.LinearTriInterpolator(tri,Vt)(lon_bathy[::Nd,::Nd],lat_bathy[::Nd,::Nd])
    
    
    # hq=m.quiver(lon_bathy[::Nd,::Nd].ravel(),lat_bathy[::Nd,::Nd].ravel(),SF*U[::Nd,::Nd].ravel(),SF*V[::Nd,::Nd].ravel(),transform=geo,angles='xy',scale=1.0,pivot='middle')
    
    hi=m.tricontourf(tri,Hs,cmap='jet',transform=geo,levels=np.linspace(0,5,21))
    hq=m.quiver(lon_bathy[::Nd,::Nd].ravel(),lat_bathy[::Nd,::Nd].ravel(),
                SF*U.ravel(),SF*V.ravel(),transform=geo,angles='xy',scale=1.0,pivot='middle')
    hc=colorbar(hi)
    hc.ax.tick_params(labelsize=18)
    hc.set_label('Hs (m)',fontsize=18)

            
    title(datetime.fromtimestamp(tc,tz=pytz.UTC).strftime('%Y/%m/%d %H:%M:%S'),fontsize=16)
    
    gcf().set_size_inches((24/2.54,18/2.54))
    dv=datetime.fromtimestamp(tc,tz=pytz.UTC).timetuple()[0:6]
    savefig('Raw/WW3_Post_%04d%02d%02d_%02d%02d%02d.png'%(dv),format='png',dpi=dpi,bbox_inches='tight')

    pause(0.01)
    hc.remove()
    for coll in hi.collections: 
        gca().collections.remove(coll) 
    draw()

    

f.close()
